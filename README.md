# Data for "Evaluation of FRET X for Single-Molecule Protein Fingerprinting"
This auxiliary repository contains large datasets for our paper on FRET X fingerprinting. It can be downloaded in the correct folder using a script in the main repository for this project: https://github.com/cvdelannoy/FRET_X_fingerprinting_simulation
